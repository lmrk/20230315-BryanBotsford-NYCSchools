package com.example.nycschools.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.R
import com.example.nycschools.model.School
import com.example.nycschools.ui.main.MainViewModel

class SchoolListAdapter(
    private val layoutInflater: LayoutInflater,
    private val viewModel: MainViewModel
) : RecyclerView.Adapter<SchoolListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = layoutInflater.inflate(R.layout.card_school, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = viewModel.schools.value?.get(position)
        viewHolder.card.setOnClickListener {
            viewModel.fetchSatScores(item?.dbn)
        }
        viewHolder.bindData(viewModel.schools.value?.get(position))
    }

    override fun getItemCount(): Int = viewModel.schools.value?.size ?: 0

    class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {

        val card: CardView = view.findViewById(R.id.school_card)

        private var name: TextView = view.findViewById(R.id.name_text_view)

        private var city: TextView = view.findViewById(R.id.city_text_view)

        private var id: String? = null

        fun bindData(data: School?) {
            id = data?.dbn
            name.text = data?.schoolName
            city.text = data?.city
        }
    }
}