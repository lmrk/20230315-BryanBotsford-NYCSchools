package com.example.nycschools.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.model.SatScores
import com.example.nycschools.model.Schools
import com.example.nycschools.service.SchoolService
import kotlinx.coroutines.launch
import retrofit2.Response

class MainViewModel(private var service: SchoolService = SchoolService()) : ViewModel() {

    private val _schools = MutableLiveData<Schools>()

    val schools: LiveData<Schools> = _schools

    private val _satScores = MutableLiveData<SatScores>()

    val satScores: LiveData<SatScores> = _satScores

    var schoolDataHasError = MutableLiveData<Unit>()

    var satDataHasError = MutableLiveData<Unit>()

    fun fetchSchools() {
        viewModelScope.launch {
            val response: Response<Schools> = service.getSchools()
            println("Response: $response")
            if (response.isSuccessful) {
                _schools.value = response.body()
            } else {
                schoolDataHasError.value = Unit
            }
        }
    }

    fun fetchSatScores(dbn: String?) {
        if (dbn != null) {
            viewModelScope.launch {
                println("DBN: $dbn")
                val response: Response<SatScores> = service.getSatScore(dbn)
                println("Response: $response")
                if (response.isSuccessful) {
                    _satScores.value = response.body()
                } else {
                    satDataHasError.value = Unit
                }
            }
        } else {
            println("dbn cannot be empty")
        }
    }
}