package com.example.nycschools.ui.main

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.R
import com.example.nycschools.adapter.SchoolListAdapter
import com.example.nycschools.model.SatScore

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    private var recyclerView: RecyclerView? = null

    private var adapter: SchoolListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        viewModel.fetchSchools()
        configureViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        configureRecyclerView(view)
        return view
    }

    private fun configureViewModel() {
        viewModel.schools.observe(this) {
            configureAdapter()
        }

        viewModel.schoolDataHasError.observe(this) {
            println("schoolDataHasError")
            displayErrorView()
        }

        viewModel.satDataHasError.observe(this) {
            println("satDataHasError")
            showErrorPopup()
        }

        viewModel.satScores.observe(this) {
            if (it.isNotEmpty()) {
                showSatScores(it[0])
            } else {
                showErrorPopup()
            }
        }
    }

    private fun configureRecyclerView(view: View) {
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = LinearLayoutManager(context)
    }

    private fun configureAdapter() {
        if (adapter == null) {
            adapter = SchoolListAdapter(LayoutInflater.from(context), viewModel)
            recyclerView?.adapter = adapter
        }
    }

    @SuppressLint("InflateParams")
    private fun showSatScores(data: SatScore) {
        val view = configureSatScoreView(data)
        createPopup(view, "SAT Scores")
    }

    private fun createPopup(view: View? = null, title: String) {
        val alert = AlertDialog.Builder(context)
        if (view != null) {
            alert.setView(view)
        }
        alert.setTitle(title)
        alert.setPositiveButton("Close") { _, _ -> }
        alert.create()
        alert.show()
    }

    private fun configureSatScoreView(data: SatScore): View {
        val view = LayoutInflater.from(context).inflate(R.layout.alert_dialog, null)
        view.findViewById<TextView>(R.id.school_name_text_view).text = "School Name: ${data.schoolName}"
        view.findViewById<TextView>(R.id.num_test_takers_text_view).text = "People who took the test count: ${data.numOfSatTestTakers}"
        view.findViewById<TextView>(R.id.reading_score_text_view).text = "Reading Score: ${data.satCriticalReadingAvgScore}"
        view.findViewById<TextView>(R.id.writing_score_text_view).text = "Writing Score: ${data.satWritingAvgScore}"
        view.findViewById<TextView>(R.id.math_score_text_view).text = "Math Score: ${data.satMathAvgScore}"
        return view
    }

    private fun displayErrorView() {

    }

    private fun showErrorPopup() {
        createPopup(title = "No Information Found for the selected school")
    }
}