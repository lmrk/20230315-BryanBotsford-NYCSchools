package com.example.nycschools.client

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetroFitClient {
    private const val BASE_URL = "https://data.cityofnewyork.us"

    private val okHttpClient: OkHttpClient =
        OkHttpClient().newBuilder().build()

    fun getInstance(): Retrofit =
        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}