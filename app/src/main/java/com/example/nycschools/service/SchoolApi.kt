package com.example.nycschools.service

import com.example.nycschools.client.RetroFitClient
import com.example.nycschools.model.SatScores
import com.example.nycschools.model.Schools
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApi {
    @GET("/resource/s3k6-pzi2.json")
    suspend fun getSchools(): Response<Schools>

    @GET("/resource/f9bf-2cp4.json")
    suspend fun getSatScore(@Query("dbn") dbn: String): Response<SatScores>
}

class SchoolService(
    private val instance: Retrofit = RetroFitClient.getInstance()
) {
    suspend fun getSchools(): Response<Schools> =
        instance.create(SchoolApi::class.java).getSchools()

    suspend fun getSatScore(dbn: String): Response<SatScores> =
        instance.create(SchoolApi::class.java).getSatScore(dbn)
}