package com.example.nycschools.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschools.MainDispatcherRule
import com.example.nycschools.model.SatScores
import com.example.nycschools.model.Schools
import com.example.nycschools.service.SchoolService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response

@OptIn(ExperimentalCoroutinesApi::class)
class MainViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var sut: MainViewModel

    @MockK
    private lateinit var mService: SchoolService

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun testInit_fetchSchools_success() = runTest {
        val mResponse: Response<Schools> = mockk()
        val mBody: Schools = mockk()
        every { mResponse.isSuccessful } returns true
        every { mResponse.body() } returns mBody
        coEvery { mService.getSchools() } returns mResponse

        sut = MainViewModel(mService)
        sut.fetchSchools()

        coVerify { mService.getSchools() }
        Assert.assertEquals(mBody, sut.schools.value)
    }

    @Test
    fun testInit_fetchSchools_failure() = runTest {
        val mResponse: Response<Schools> = mockk()
        val mBody: Schools = mockk()
        every { mResponse.isSuccessful } returns false
        every { mResponse.body() } returns mBody
        coEvery { mService.getSchools() } returns mResponse

        sut = MainViewModel(mService)
        sut.fetchSchools()

        coVerify { mService.getSchools() }
        Assert.assertNull(sut.schools.value)
        Assert.assertNotNull(sut.schoolDataHasError.value)
    }

    @Test
    fun testInit_fetchSatScores_success() = runTest {
        val expectedDbn = "937hhd"
        val mResponse: Response<SatScores> = mockk()
        val mBody: SatScores = mockk()
        every { mResponse.isSuccessful } returns true
        every { mResponse.body() } returns mBody
        coEvery { mService.getSatScore(expectedDbn) } returns mResponse

        sut = MainViewModel(mService)
        sut.fetchSatScores(expectedDbn)

        coVerify { mService.getSatScore(expectedDbn) }
        Assert.assertEquals(mBody, sut.satScores.value)
    }

    @Test
    fun testInit_fetchSatScores_failure() = runTest {
        val expectedDbn = "937hhd"
        val mResponse: Response<SatScores> = mockk()
        val mBody: SatScores = mockk()
        every { mResponse.isSuccessful } returns false
        every { mResponse.body() } returns mBody
        coEvery { mService.getSatScore(expectedDbn) } returns mResponse

        sut = MainViewModel(mService)
        sut.fetchSatScores(expectedDbn)

        coVerify { mService.getSatScore(expectedDbn) }
        Assert.assertNull(sut.satScores.value)
        Assert.assertNotNull(sut.satDataHasError.value)
    }
}