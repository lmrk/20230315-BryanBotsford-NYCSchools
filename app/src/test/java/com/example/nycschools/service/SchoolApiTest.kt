package com.example.nycschools.service

import com.example.nycschools.client.RetroFitClient
import com.example.nycschools.model.SatScores
import com.example.nycschools.model.Schools
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.mockkObject
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import retrofit2.Retrofit

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolApiTest {

    private lateinit var sut: SchoolService

    @MockK
    private lateinit var mInstance: Retrofit

    @MockK
    private lateinit var mApi: SchoolApi

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        mockkObject(RetroFitClient)

        every { RetroFitClient.getInstance() } returns mInstance

        sut = SchoolService()
    }

    @Test
    fun test_getSchools() = runTest {
        val mResponse: Response<Schools> = mockk()
        every { mInstance.create(SchoolApi::class.java) } returns mApi
        coEvery { mApi.getSchools() } returns mResponse

        val result = sut.getSchools()

        Assert.assertEquals(mResponse, result)
    }

    @Test
    fun test_getSatScore() = runTest {
        val expectedDbn = "89daf"
        val mResponse: Response<SatScores> = mockk()
        every { mInstance.create(SchoolApi::class.java) } returns mApi
        coEvery { mApi.getSatScore(expectedDbn) } returns mResponse

        val result = sut.getSatScore(expectedDbn)

        Assert.assertEquals(mResponse, result)
    }
}