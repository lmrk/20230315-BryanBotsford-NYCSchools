package com.example.nycschools.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.LiveData
import com.example.nycschools.R
import com.example.nycschools.model.School
import com.example.nycschools.model.Schools
import com.example.nycschools.ui.main.MainViewModel
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SchoolListAdapterTest {

    private lateinit var sut: SchoolListAdapter

    @MockK
    private lateinit var mLayoutInflater: LayoutInflater

    @MockK
    private lateinit var mViewModel: MainViewModel

    @MockK
    private lateinit var mLiveData: LiveData<Schools>

    @MockK
    private lateinit var mView: View

    @MockK
    private lateinit var mViewGroup: ViewGroup

    @MockK
    private lateinit var mViewHolder: SchoolListAdapter.ViewHolder

    @MockK
    private lateinit var mCardView: CardView

    @MockK
    private lateinit var mNameTextView: TextView

    @MockK
    private lateinit var mCityTextView: TextView

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        every { mView.findViewById<CardView>(R.id.school_card) } returns mCardView
        every { mView.findViewById<TextView>(R.id.name_text_view) } returns mNameTextView
        every { mView.findViewById<TextView>(R.id.city_text_view) } returns mCityTextView
        every { mLayoutInflater.inflate(R.layout.card_school, mViewGroup, false) } returns mView

        sut = SchoolListAdapter(mLayoutInflater, mViewModel)
    }

    @Test
    fun test_onCreateViewHolder() {
        val result = sut.onCreateViewHolder(mViewGroup, 0)

        Assert.assertEquals(mView, result.itemView)
    }

    @Test
    fun test_onBindViewHolder() {
        val mSchool: School = mockk()
        val schools = Schools()
        schools.add(mSchool)
        every { mLiveData.value } returns schools
        every { mViewModel.schools } returns mLiveData
        every { mViewHolder.card } returns mCardView

        sut.onBindViewHolder(mViewHolder, 0)

        verify { mCardView.setOnClickListener(any()) }
    }
}